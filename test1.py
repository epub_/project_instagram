#  pip install selenium
#  качаешь драйвер для своей версии хрома https://sites.google.com/a/chromium.org/chromedriver/downloads
from selenium import webdriver
from time import sleep
from selenium.webdriver.common.keys import Keys
import requests
import urllib.parse
import json

path = "C:/Users/alexi/Documents/Project_of_VVPD/chromedriver_win32/chromedriver.exe" # Путь до драйвера который скачал

driver = webdriver.Chrome(path)
driver.get("https://www.instagram.com/accounts/login")
sleep(2)

login = driver.find_element_by_name("username")
login.send_keys("")
sleep(1)

password = driver.find_element_by_name("password")
password.send_keys("")
sleep(1)

btn1 = driver.find_element_by_class_name("L3NKy")
btn1.click()
sleep(4)

driver.get("https://www.instagram.com/carssed01/")
sleep(1)
url_base = "https://www.instagram.com/graphql/query/?"

after = None
index = 1
folowers_in_progres = 0
while True:
    after_value = f', "after": "{after}"' if after else ''
    variables = f'{{"id":"6647521888","include_reel":true,"fetch_mutual":true,"first":50{after_value}}}'
    get_params = {
        "query_hash": "c76146de99bb02f6415203be841dd25a",
        "variables": variables
    }
    ws_url = url_base + urllib.parse.urlencode(get_params)
    driver.get(ws_url)
    sleep(3)
    json_inf = driver.find_element_by_xpath("/html/body/pre")
    finished_text = json.loads(json_inf.text)

    sleep(3)
    
    try:
        data = json.load(open(f'pack_{index}.json'))
    except:
        data = {}
    data = {'json_inf': finished_text}
    with open(f'pack_{index}.json', 'w', encoding='utf-8') as file:
        json.dump(data, file)

    with open(f'pack_{index}.json', 'r', encoding='utf-8') as f:
        data = json.load(f)

    if not data['json_inf']['data']['user']['edge_followed_by']['page_info']['has_next_page']:
        break
    
    after = data['json_inf']['data']['user']['edge_followed_by']['page_info']['end_cursor']
    all_folowers = data['json_inf']['data']['user']['edge_followed_by']['count']
    in_current_pag = len(data['json_inf']['data']['user']['edge_followed_by']['edges'])
    folowers_in_progres += in_current_pag
    print(f'Обработано {folowers_in_progres}/{all_folowers}')

    sleep(5 if index % 10 != 0 else 15)
    index += 1

all_documents = index
sleep(5)
print('Succesfull')
driver.quit()



